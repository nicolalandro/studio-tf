import tensorflow as tf

x_train = [[0, 1, 0], [1, 0, 1]]
y_train = [0, 1]

x_test = [[1, 1, 1], [0, 0, 0]]
y_test = [1, 0]

# Convert the inputs to a Dataset.
dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))

iter = dataset.make_initializable_iterator()
el = iter.get_next()

with tf.Session() as sess:
    sess.run(iter.initializer)
    print(sess.run(el))
    print(sess.run(el))
