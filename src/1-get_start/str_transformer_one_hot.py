import tensorflow as tf

alphabet = tf.string_split(tf.constant('abcdef', shape=[1]), delimiter="").values


def splitter(i):
    b = tf.string_split(i, delimiter="").values
    str_to_arr = lambda x: tf.map_fn(lambda c: tf.cast(tf.equal(x, c), dtype=tf.int32), alphabet, dtype=tf.int32)
    return tf.map_fn(str_to_arr, b, dtype=tf.int32)


with tf.Session() as sess:
    a = tf.constant(["abc", "def"], shape=[2, 1])

    print(tf.map_fn(splitter, a, dtype=tf.int32).eval())
