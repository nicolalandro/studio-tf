import tensorflow as tf


def splitter(i):
    b = tf.string_split(i, delimiter="").values
    str_to_arr = lambda x: tf.equal(x, x)
    return tf.map_fn(str_to_arr, b, dtype=tf.bool)


with tf.Session() as sess:
    a = tf.constant(["abc", "def"], shape=[2, 1])

    print(tf.map_fn(splitter, a, dtype=tf.bool).eval())
