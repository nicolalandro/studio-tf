import tensorflow as tf

x = tf.placeholder(tf.float32, shape=[2, 3])


def cond(batch, output, i):
    return tf.less(i, tf.shape(batch)[0])


def body(batch, output, i):
    output = output.write(i, tf.add(batch[i], 10))
    return batch, output, i + 1


# TensorArray is a data structure that support dynamic writing
output_ta = tf.TensorArray(dtype=tf.float32,
                           size=0,
                           dynamic_size=True,
                           element_shape=(x.get_shape()[1],))
_, output_op, _ = tf.while_loop(cond, body, [x, output_ta, 0])
output_op = output_op.stack()

with tf.Session() as sess:
    print(sess.run(output_op, feed_dict={x: [[1, 2, 3], [0, 0, 0]]}))

