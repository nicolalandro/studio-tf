import numpy as np
import tensorflow as tf
from tensorflow.contrib.boosted_trees.estimator_batch.estimator import GradientBoostedDecisionTreeClassifier
from tensorflow.contrib.boosted_trees.proto import learner_pb2 as gbdt_learner

x_train = np.array([np.array([0, 1, 0], np.int32), np.array([1, 0, 1], np.int32)])
y_train = np.array([0, 1], np.int32)

x_test = [[1, 1, 1], [0, 0, 0]]
y_test = [1, 0]

input_fn = tf.estimator.inputs.numpy_input_fn(
    x=x_train,
    y=y_train,
    batch_size=100,
    num_epochs=None,
    shuffle=True
)

examples_per_layer = 1000

learner_config = gbdt_learner.LearnerConfig()
learner_config.learning_rate_tuner.fixed.learning_rate = 0.1
learner_config.regularization.l1 = 0
learner_config.regularization.l2 = 1 / examples_per_layer
learner_config.constraints.max_tree_depth = 16
growing_mode = gbdt_learner.LearnerConfig.LAYER_BY_LAYER
learner_config.growing_mode = growing_mode
learner_config.multi_class_strategy = gbdt_learner.LearnerConfig.DIAGONAL_HESSIAN

run_config = tf.contrib.learn.RunConfig(save_checkpoints_secs=300)

gbdt_model = GradientBoostedDecisionTreeClassifier(
    model_dir=None,  # No save directory specified
    learner_config=learner_config,
    n_classes=2,
    examples_per_layer=examples_per_layer,
    num_trees=2,
    center_bias=False,
    config=run_config
)

gbdt_model.fit(input_fn=input_fn, max_steps=1)
