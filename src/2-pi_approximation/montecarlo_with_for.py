import random
import time

length = int(1e5)
c = [0.0, 0.0]
r = 1.0
four = 4.0

start_time = time.time()
hits = 0.0
for i in range(length):
    p = [random.uniform(-1, 1), random.uniform(-1.0, 1.0)]
    d2 = ((p[0] - c[0]) * (p[0] - c[0])) + ((p[1] - c[1]) * (p[1] - c[1]))
    if d2 <= r * r:
        hits += 1

frac = hits / length
pi = four * frac
print(pi)
print('millisecond:', int(round((time.time() - start_time) * 1000)))
