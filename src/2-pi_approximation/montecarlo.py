import time

import tensorflow as tf

parallel = 2560
length = int(1e5)
c = tf.constant([0, 0], dtype=tf.float32)
r = tf.constant(1, dtype=tf.float32)
four = tf.constant(4, dtype=tf.float32)
random = tf.random_uniform((length, 2), minval=-1, maxval=1, dtype=tf.float32)
px = tf.constant([1, 1], dtype=tf.float32, )


def is_in_cirche(points, C, R2):
    is_in = lambda p: tf.cast(tf.less_equal(square_euclidean_distance(p, C), R2), dtype=tf.float32)

    return tf.map_fn(is_in, points, dtype=tf.float32,
                     infer_shape=False, parallel_iterations=parallel)


def square_euclidean_distance(p1, p2):
    return tf.reduce_sum(tf.square(p1 - p2))


start_time = time.time()
with tf.Session() as sess:
    #print('hits..')
    hits = is_in_cirche(random, c, tf.square(r))
    #print('frac..')
    frac = tf.reduce_sum(hits) / tf.constant(length, dtype=tf.float32)
    #print('pi..')
    pi = four * frac
    #print('eval:', int(round((time.time() - start_time) * 1000)))
    val = pi.eval()
    print('pi:', val)

print('millisecond:', int(round((time.time() - start_time) * 1000)))
