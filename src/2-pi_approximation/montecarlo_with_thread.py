import random
import threading
import time


class InCircle(threading.Thread):
    def __init__(self, result, pos, c, r):
        super(InCircle, self).__init__()
        self.r = r
        self.c = c
        self.pos = pos
        self.result = result

    def run(self):
        p = [random.uniform(-1, 1), random.uniform(-1.0, 1.0)]
        d2 = ((p[0] - self.c[0]) * (p[0] - self.c[0])) + ((p[1] - self.c[1]) * (p[1] - self.c[1]))
        self.result[self.pos] = 1 if d2 <= self.r * self.r else 0


length = int(1e5)
c = [0.0, 0.0]
r = 1.0
four = 4.0
result = [0] * length
threads = []

start_time = time.time()
for i in range(length):
    t = InCircle(result, i, c, r)
    t.start()
    threads.append(t)
for t in threads:
    t.join()
hits = float(sum(result))


frac = hits / length
pi = four * frac
print(pi)
print('millisecond:', int(round((time.time() - start_time) * 1000)))
