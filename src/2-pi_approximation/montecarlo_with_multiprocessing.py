import multiprocessing as mp
import random
import time

length = int(1e5)
c = [0.0, 0.0]
r = 1.0
four = 4.0


def compute():
    p = [random.uniform(-1, 1), random.uniform(-1.0, 1.0)]
    d2 = ((p[0] - c[0]) * (p[0] - c[0])) + ((p[1] - c[1]) * (p[1] - c[1]))
    return 1 if d2 <= r * r else 0


pool = mp.Pool(4)
start_time = time.time()
future_res = [pool.apply_async(compute) for _ in range(length)]
result = [f.get() for f in future_res]
hits = float(sum(result))
frac = hits / length
pi = four * frac
print(pi)
print('millisecond:', int(round((time.time() - start_time) * 1000)))
